variable "create_script" {
  default = true
}

variable "script_name" {
}

variable "script_template_file" {
}

variable "script_template_vars" {
  type = map(string)
  default = {}
}
