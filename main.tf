
data "template_file" "script" {
  count = var.create_script ? 1 : 0

  template = file(var.script_template_file)

  vars     = var.script_template_vars
}

resource "local_file" "script" {
  count = var.create_script ? 1 : 0

  filename = var.script_name
  content  = data.template_file.script[count.index].rendered
}

resource "null_resource" "script" {
  count    = var.create_script ? 1 : 0
  triggers = {
    script = sha256(data.template_file.script[count.index].rendered)
  }

  provisioner "local-exec" {
    command = "chmod +x ${var.script_name}"
  }

  depends_on = [
    local_file.script
  ]
}
