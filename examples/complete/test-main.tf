terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-devops-script-complete"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../../"

  script_name = "some-name.sh"
  script_template_file = "script-template.sh.tpl"
  script_template_vars = {
    script_name = "some-name.sh"
  }
}
