terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-devops-script-simple"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../../"

  create_script = false

  script_name = "some-name"
  script_template_file = "some-file"
}
